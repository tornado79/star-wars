# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

 This project is a very simple ​React.js app about Star Wars heroes and movies.
 It is the first version, that only fetches movies and people and also searches realtime for people.

### How do I get set up? ###

* 1.  npm install and all dependencies will download local.
* 2.  npm run production will create the dist folder.
* 3.  npm start will start the http-server on localhost:8080 , for testing.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* theodoreme@gmail.com
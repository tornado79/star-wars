/**
 * Created by lenovo on 04-Feb-17.
 */
var React = require('react');
var helpers = require('../utils/helpers');
var styles = require('../styles/styles');

import { browserHistory, Link } from 'react-router'
import { Panel } from 'react-bootstrap';


var Character = React.createClass({
    /*INIT STATE*/
    getInitialState:function(){
        return {
            character:'',
            loaded:false
        }
    },
    componentDidMount:function(){
        /*GET THE OBJECT FROM THE URL PARAM*/
        const tmp =  JSON.parse(this.props.location.query.character);
        this.setState({character:tmp,loaded:true});//parse the character parameter)

        /* NOT NEED TO CALL THE API
        helpers.getCharacter(this.props.params.id)
            .then(results => {
                this.setState({character:results.data,loaded:true})
            })
            .catch(function(err){
                console.warn('error while fetching the character'+err);
            });*/
    },
    goBack:function(){
        browserHistory.goBack();
    },
    render:function(){
        return(
            <div>
                <a href='#' onClick={this.goBack} style={styles.backPosition}>Back</a>
                <div>
                    {this.state.loaded ?
                        <div>
                            <span><b>{this.state.character.name}</b></span>
                            <Panel>
                                <ul>
                                    <li>Gender:{this.state.character.gender}</li>
                                    <li>Height:{this.state.character.height}</li>
                                    <li>Skin Color:{this.state.character.skin_color}</li>
                                    <li>Eyes Color:{this.state.character.eye_color}</li>
                                    <li>Movies:</li>
                                    <ol>
                                        {this.state.character.films.map((m, i) =>
                                            <li key={i}><a href={m}>{m}</a></li>
                                        )}
                                    </ol>
                                </ul>

                            </Panel>
                        </div>
                        : <h1>Loading</h1>}
                </div>
            </div>
        )
    }
});


export default Character

/**
 * Created by lenovo on 03-Feb-17.
 */
var React = require('react');
var helpers = require('../utils/helpers');
var loader = require('../static/loader.gif');
var styles = require('../styles/styles');

import { browserHistory, Link } from 'react-router'
import InputSearch from './InputSearch'

var PeopleList = React.createClass({
    /*INIT STATE*/
    getInitialState:function(){
        return {
            people:'',
            searchTxt:'',
            loaded:false
        }
    },
    componentDidMount:function(){
        /*CALL API TO GET PEOPLE*/
        helpers.getPeople()
            .then(results => {
                this.setState({people:results.data,loaded:true})
            })
    },
    handleClick:function(item,event) {
        event.preventDefault();
        const path = `/character/${item.target.id}`;
        /* Go to character/: path*/
        browserHistory.push(path);
    },
    handleChange:function(item){
        var search = item.target.value;
        this.setState({loaded:false});//update state  && show loader gif
        /*CALL API TO SEARCH PEOPLE*/
        helpers.searchPeople(item.target.value)
            .then(results => {
                this.setState({people:results.data,searchTxt:search,loaded:true})
            })
    },
    render:function(){
        if(this.state.loaded){
            {/*CREATE THE URLS LIKE: character/:id?characterObject */}
            var data = this.state.people.results.map((item,index)=>  { return <li style={styles.position} key={index.toString()}><Link to={{pathname:`/character/${++index}`,query:{'character':JSON.stringify(item)}}}>{item.name}></Link></li>});
        }
        return(
            this.state.loaded ?
                <div style={styles.listsPosition}>
                    <InputSearch value={this.state.searchTxt} onChange={this.handleChange}/>
                    <div>{data}</div>
                </div>
                : <img style={styles.imgposition} src={loader}/>
        )
    }
});

export default PeopleList


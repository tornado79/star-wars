/**
 * Created by lenovo on 04-Feb-17.
 */
import React from 'react'

var helpers = require('../utils/helpers');
var styles = require('../styles/styles');
import Moment from 'react-moment';
import { browserHistory } from 'react-router';
import { Panel } from 'react-bootstrap';

export default React.createClass({
    /*INIT STATE*/
    getInitialState:function(){
        return {
            movie:'',
            loaded:false
        }
    },
    componentDidMount:function(){
        /*GET THE OBJECT FROM THE URL PARAM*/
        const tmp =  JSON.parse(this.props.location.query.movie);
        this.setState({movie:tmp,loaded:true});//parse the movie parameter)
        
        /* NOT NEED TO CALL THE API
        movieid = this.props.params.id;
        helpers.getMovie(movieid)
            .then(results => {
                this.setState({movie:results.data,loaded:true})
            })
            .catch(function(err){
                console.warn('error while fetching the movie'+err);
            });*/
    },
    goBack:function(){
        browserHistory.goBack();
    },
    render:function() {
        var portionCharacters=Array(3);//get the three first movies (as on PDF)
        return (
            <div>
                <a href='#' onClick={this.goBack} style={styles.backPosition}>Back</a>
                <div>
                    {this.state.loaded ?
                    <div>
                        <span><b>{this.state.movie.title}</b></span>
                        <ul>
                            <li>Release Date: <Moment format="DD/MM/YYYY">{this.state.movie.created}</Moment></li>
                            <li>Director: {this.state.movie.director}</li>
                        </ul>
                        <Panel>
                            {this.state.movie.opening_crawl}
                        </Panel>
                        <Panel header="Characters">
                            <ol>
                                {this.state.movie.characters.slice(0,3).map((m, i) =>
                                    <li key={i}><a href={m}>{m}</a></li>
                                )}
                            </ol>

                        </Panel>
                    </div>
                        : <h1>Loading</h1>}
                </div>
            </div>
        )
    }
})

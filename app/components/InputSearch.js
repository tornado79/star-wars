/**
 * Created by lenovo on 05-Feb-17.
 */
var React = require('react');
var helpers = require('../utils/helpers');
var styles = require('../styles/styles');

var InputSearch = React.createClass({
    componentDidMount:function(){
        this.searchInput.focus();//keep the search input focus
    },
    render:function(){
        return(
           <input ref={(input) => { this.searchInput = input; }}
                  type='text'  value={this.props.value} placeholder='search character' onChange={this.props.onChange}/>
        )
    }
});

export default InputSearch



/**
 * Created by lenovo on 03-Feb-17.
 */
var React = require('react');
var helpers = require('../utils/helpers');
var loader = require('../static/loader.gif');
var styles = require('../styles/styles');

import { browserHistory, Link } from 'react-router'


var MoviesList = React.createClass({
    /*INIT STATE*/
    getInitialState:function(){
        return {
            movies:'',
            loaded:false
        }
    },
    componentDidMount:function(){
        /*CALL API TO GET MOVIES*/
        helpers.getMovies()
            .then(results => {
                this.setState({movies:results.data,loaded:true})
            })
    },
    handleClick:function(item,event) {
        event.preventDefault();
        const path = `/movie/${item.target.id}`;
        /* Go to movie/: path*/
        browserHistory.push(path);
    },
    render:function(){
        if(this.state.loaded){
            {/*CREATE THE URLS LIKE: movie/:id?movieObject */}
            var data = this.state.movies.results.map(item=>  <li style={styles.position} key={item.episode_id.toString()}><Link to={{pathname:`/movie/${item.episode_id}`,query:{movie:JSON.stringify(item)}}}>{item.title}</Link></li>);
        }
        return(
        this.state.loaded ? <div style={styles.listsPosition}>{data}</div> : <img style={styles.imgposition} src={loader}/>
        )
    }
});


export default MoviesList

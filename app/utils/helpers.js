/**
 * Created by lenovo on 03-Feb-17.
 */
var Axios = require('axios');

var helpers = {
    getMovies:function(){
        return Axios.get('http://swapi.co/api/films/')
            .catch(function(err){
                console.warn('error while fetching the movies'+err);
            });
    },
    getMovie:function(value){
      return Axios.get('http://swapi.co/api/films/'+value+'/')
          .catch(function(err){
              console.warn('error while fetching the movie'+err);
          });
    },
    getPeople:function(){
        return Axios.get('http://swapi.co/api/people/')
            .catch(function(err){
                console.warn('error while fetching the people'+err);
            });
    },
    getCharacter:function(value){
        return Axios.get('http://swapi.co/api/people/'+value)
            .catch(function(err){
                console.warn('error while fetching the people'+err);
            });
    },
    searchPeople:function(value){
        return Axios.get('https://swapi.co/api/people/?search='+value)
            .catch(function(err){
                console.warn('error while searching people'+err);
            });
    }

};

module.exports = helpers;
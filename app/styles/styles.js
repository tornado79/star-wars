/**
 * Created by lenovo on 04-Feb-17.
 */

var styles = {
    position: {
        'textAlign': 'left'
    },
    imgposition:{
        'display':'block',
        'margin':'0 auto'
    },
    backPosition:{
        'display':'block',
        'textAlign':'center',
        'padding': '5px'
    },
    listsPosition:{
         'padding': '40px'
        }
};

module.exports = styles;
/**
 * Created by lenovo on 03-Feb-17.
 */

import { Nav, NavItem } from 'react-bootstrap';
import {Router, Route, browserHistory , Link, Redirect} from 'react-router'

import './styles/styles.css';
var helpers = require('./utils/helpers');
import MoviesList from './components/MoviesList';//movies list component
import Movie from './components/Movie';
import PeopleList from './components/PeopleList';//people list component
import Character from './components/Character';
import NotFound from './components/NotFound';
var React = require('react');
var ReactDOM = require('react-dom');

var StarWarsContainer = React.createClass({
    getInitialState:function(){
        return {
            movies:'',
            people:'',
            activeTab:0,
            loaded:false
        }
    },
    componentDidMount:function(){
       /* helpers.getMovies().then(results => this.setState({movies:results.data,moviesLoaded:true}));
        helpers.getPeople().then(results => this.setState({people:results.data,peopleLoaded:true}));*/
        helpers.getMovies()
            .then(results => {
                this.setState({movies:results.data});
                return helpers.getPeople();
            })
            .then(results => this.setState({people:results.data,loaded:true}));
    },
    handleSelect:function(eventKey){
        //event.preventDefault();
        this.setState({activeTab:eventKey});
    },
    movieDetails:function(){
        console.log('you clicked');
        return <div>Movie details</div>;
    },
    render:function(){
        const pageTitle = "Star Wars";
        const tabs = [{key:0,name:'Movies'},{key:1,name:'People'}];
        return(
            <div className='container'>
                <div className="row">
                    <div className="col-sm-2" ></div>
                    <div className="col-sm-8 text-center" >
                        <h1>{pageTitle}</h1>
                        {/*<Tabs activeKey={this.state.activeTab} onSelect={this.handleSelect} id="controlled-tab-example">
                            <Tab eventKey={tabs[0].key} title={tabs[0].name}>
                                {this.state.loaded == true ? <MoviesList movies={this.state.movies} onClick={this.movieDetails}/> : <img src={loader}/> }
                            </Tab>
                            <Tab eventKey={tabs[1].key} title={tabs[1].name}>
                                {this.state.loaded == true ? <PeopleList people={this.state.people} /> : <img src={loader}/> }
                            </Tab>
                        </Tabs>*/}
                        <Nav bsStyle="tabs" activeKey={this.state.activeTab} onSelect={this.handleSelect}>
                            <NavItem eventKey={tabs[0].key} ><Link to="/movies">{tabs[0].name}</Link></NavItem>
                            <NavItem eventKey={tabs[1].key} ><Link to="/people">{tabs[1].name}</Link></NavItem>
                        </Nav>
                    </div>
                    <div className="col-sm-2" ></div>
                </div>
                <div className="row">
                    <div className="col-sm-2"></div>
                    <div className="col-sm-8">
                        {/*render children components here*/}
                        {this.props.children}
                    </div>
                    <div className="col-sm-2"></div>
                </div>
            </div>
        )
    }
});
ReactDOM.render(
    <Router history={browserHistory}>
        {/*Redirect to default load Movies*/}
        <Redirect from="/" to="/movies"/>
        <Route path='/' component={StarWarsContainer}>
            <Route path='/movies'  component={MoviesList}></Route>
            <Route path='/movie/:id'  component={Movie}></Route>
            <Route path='/people'  component={PeopleList}></Route>
            <Route path='/character/:id'  component={Character}></Route>
            <Route path='*' component={NotFound} />
        </Route>
    </Router>,
    document.getElementById('root'));
